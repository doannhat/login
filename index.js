var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var _ = require('underscore');
var loginToken = false;
var Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
var md5 = require('blueimp-md5');

app.post("/register", jsonParser, (req, res) => {
    var email = req.body.email;
    var password = req.body.password;
    var confirm = req.body.password_confirm;
    if (password !== confirm) {
        console.error("Password not match!")
        res.end("Password not match!");
    } else {
        fs.readFileAsync(__dirname + "/data.json").then((data) => { 
            let listUsers = JSON.parse(data);
            console.log(data);
            let newuser = {
                email : `${email}`,
                password : `${password}`
            }
            listUsers.users.push(newuser);
            console.log(listUsers);
            let parseListUser = JSON.stringify(listUsers);
            return parseListUser;
        }).then(function (data) {
            fs.writeFile(__dirname + "/data.json", data, 'utf8', (err) => {
                if (err) {
                    throw err;
                } else {
                    res.end("User data is written successfully.");
                }
            })
        }); 
    }
});

app.get("/login", jsonParser, (req, res) => {
    fs.readFileAsync(__dirname + "/data.json", 'utf8', (err, data) => {
        if (err){
            throw err;
        }
        var body = JSON.parse(data);
        // console.log(body);
        var query = req.query;
        body.users.forEach((u) => {
            // console.log(u.email + " : " + u.password);
            // console.log(query.email + " : " + query.password);
            if ((query.email == u.email) && (query.password == u.password)) {
                res.end(JSON.stringify(u));
            }
        })
    })
});

// Update user
app.put('/update/:id', jsonParser, function (req, res) {
    fs.readFile(__dirname + "/" + "data.json", 'utf8', function (err, data) {
        var listUsers = JSON.parse(data);
        var id_edit = req.params.id;
        var body = req.body;
        console.log(typeof listUsers.users);
        listUsers.users.forEach(function (u, index) {
            if (id_edit == u.id) {
                listUsers.users[index].name = body.name;
                listUsers.users[index].password = body.password;
            }
        });
        fs.writeFileSync(__dirname + "/" + "data.json", JSON.stringify(listUsers, null ,4));
        res.end(JSON.stringify(listUsers));
    });
});

// Forgot password
app.put('/forgot-password', jsonParser, function (req, res) {
    fs.readFile(__dirname + "/" + "data.json", 'utf8', function (err, data) {
        var listUsers = JSON.parse(data);
        var body = req.body;
        var token;
        var id;
        listUsers.users.forEach(function (u, index) {
            if (body.email == u.email) {
                token = md5(body.email);
                id = u.id;
            }
        });
        if(token) {
            res.end(JSON.stringify(`http://localhost:3000/forgot-password/active?token=${token}&id=${id}`));
        } else {
            res.end(JSON.stringify(`Email không tồn tại`));
        }

    });
});

// Forgot password active
app.get('/forgot-password/active', jsonParser, function (req, res) {
    fs.readFile(__dirname + "/" + "data.json", 'utf8', function (err, data) {
        var listUsers = JSON.parse(data);
        var token;
        var query = req.query;
        var user;
        listUsers.users.forEach(function (u, index) {
            if ((query.id == u.id) && (query.token == md5(u.email))) {
                
                listUsers.users[index].password = '12345!@#';
                user = listUsers.users[index];
            }
        });
        if(user) {
            
            fs.writeFileSync(__dirname + "/" + "data.json", JSON.stringify(listUsers, null, 4));
            res.end(`Password của bạn đã được thay đổi về mặc định '12345!@#' \n 
                Bạn có thể vào link sau để đổi mật khẩu http://localhost:3000/update/${user.id}
            `);
        } else {
            res.end("Token đã hết hạn hoặc ko tồn tại");
        }
    });
});


var server = app.listen(3000, () => {
    let host = server.address().address;
    let port = server.address().port;
    console.log(`Server listening on http://${host}:${port}`);
})